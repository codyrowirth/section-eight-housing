import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Waitlist, WaitlistRelations, Property} from '../models';
import {PropertyRepository} from './property.repository';

export class WaitlistRepository extends DefaultCrudRepository<
  Waitlist,
  typeof Waitlist.prototype.id,
  WaitlistRelations
> {

  public readonly properties: HasManyRepositoryFactory<Property, typeof Waitlist.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('PropertyRepository') protected propertyRepositoryGetter: Getter<PropertyRepository>,
  ) {
    super(Waitlist, dataSource);
    this.properties = this.createHasManyRepositoryFactoryFor('properties', propertyRepositoryGetter,);
    this.registerInclusionResolver('properties', this.properties.inclusionResolver);
  }
}
