import {Entity, model, property, hasMany} from '@loopback/repository';
import {Property} from './property.model';

@model()
export class Waitlist extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  waitlistName: string;

  @property({
    type: 'boolean',
    required: true,
  })
  senior: boolean;

  @property({
    type: 'boolean',
    required: true,
  })
  disability: boolean;

  @property({
    type: 'boolean',
    required: true,
  })
  homeless: boolean;

  @property({
    type: 'boolean',
    required: true,
  })
  isOpen: boolean;

  @property({
    type: 'number',
    required: true,
  })
  applicantCount: number;

  @property({
    type: 'number',
    required: true,
  })
  avgWaitTimeMonths: number;

  @hasMany(() => Property)
  properties: Property[];

  constructor(data?: Partial<Waitlist>) {
    super(data);
  }
}

export interface WaitlistRelations {
  // describe navigational properties here
}

export type WaitlistWithRelations = Waitlist & WaitlistRelations;
