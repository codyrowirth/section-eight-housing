import {
  Filter,
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
  response,
} from '@loopback/rest';
import {Waitlist} from '../models';
import {WaitlistRepository} from '../repositories';

export class WaitlistController {
  constructor(
    @repository(WaitlistRepository)
    public waitlistRepository : WaitlistRepository,
  ) {}

  @get('/waitlists')
  @response(200, {
    description: 'Array of Waitlist model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Waitlist, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Waitlist) filter?: Filter<Waitlist>,
  ): Promise<Waitlist[]> {
    return this.waitlistRepository.find(filter);
  }
}
