module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  content: ["./src/**/*.{js,jsx}"],
  theme: {
    extend: {
      colors: {
        teal: "#2EB5BC",
        "pale-teal": "#83D1DE",
        "deep-teal": "#0C6064",
        "deepest-teal": "#0A4245",
        "vivid-orange": "#F07622",
        "pale-blue": "#E6F0F9",
        "light-gray": "#F5F7FA",
        sage: "#797969",
        gold: "#FAB517",
        navy: "#22295A"
      }
    }
  },
  variants: {},
  plugins: []
};
