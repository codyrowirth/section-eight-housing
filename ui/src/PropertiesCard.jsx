import React from "react";

// pretty much took the same principle and functionality of the waitlist cards to display property cards on the details pages
// note that not all the info on the mockup was available in the data set so it is kind of empty, but functions 

const PropertiesCard = ({ properties }) => {

  return (
    <div>
        <div className="border-2 rounded-sm p-4">
            <div className="flex-cards">
                <div className="child-card">
                    <h3 className="text-sm"><b>Property:</b> {properties.name}</h3>
                    <h3 className="text-sm"><b>State:</b> {properties.state}</h3>
                    <h3 className="text-sm"><b>City:</b> {properties.state}</h3>
                    <h3 className="text-sm"><b>Bedrooms:</b> {properties.numBedrooms}</h3>
                </div>
            </div>
        </div>
    </div>
  );
};

export default PropertiesCard;
