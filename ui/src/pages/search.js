import React, { useState, useEffect } from 'react';
import WaitlistCard from "../WaitlistCard";
import { useParams } from "react-router-dom";

// seperated out the search page from the originial app.js file so that I could add routing into the app.js file
// how I would have possibly approached this if not receiving a mock up: https://mui.com/components/data-grid/filtering/
// I feel like the real benefit of using something like data grad is that it can help you filter the data without needing to build custom logic for it
// additionally you could filter data and just make it clickable to route to a page where the unique id is routed in the URL like I have now

let Search = () => {

  // fetched the route id from the url and parsed to an integer

  const tempVar = useParams();

  let id = parseInt(tempVar.id);

  // set the state variable for the api return

  const [waitlists, setWaitlists] = React.useState([]);

  // set the state for the fuilter properties

  const [bedrooms, setBedrooms] = React.useState();
  const [location, setLocation] = React.useState();
  const [senior, setSenior] = React.useState(true);
  const [disabled, setDisabled] = React.useState(true);
  const [homeless, setHomeless] = React.useState(true);

  // managed state change for the filter properties to update on key events

  const onChangeB = (event) => {
    setBedrooms(event.target.value);
  };

  const onChangeL = (event) => {
    setLocation(event.target.value);
  };

  const onChangeS = (event) => {
    setSenior(true);
    if (senior === true) {
      setSenior(false);
    }
  };

  const onChangeD = (event) => {
    setDisabled(true);
    if (disabled === true) {
      setDisabled(false);
    }
  };

  const onChangeH = (event) => {
    setHomeless(true);
    if (homeless === true) {
      setHomeless(false);
    }
    console.log(homeless);
  };

  // implemented a function for the clear button to clear the inpout fields of their values and to route to / to refresh the page automatically

  function ClearP() {
    var inputs = document.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++)
      inputs[i].value = '';
    window.location.href = "/";
  }

  // makes the api call, use a for loop and an if statement to loop through the initial return and filter the properties based on the user input
  // I then filter out the response where the input matches the critieria from the return call

  let filter = () => {
    fetch('http://localhost:3001/waitlists?filter={"include":["properties"]}')
      .then(response => response.json())
      .then(response => {
        // honestly know that this approach is incorrect, but struggled with implementing the logic required for all 5 filters to work simultaneously
        // at first my approach was a large block if / else statements which was not very DRY
        // I then attempted to query the api via the fetch response, but could not get it to work using dynamic variables
        // http://localhost:3001/waitlists?filter={"include":["properties"]}
        // {{"include":["properties"]}"where":{"disability":true}{"where":{"homeless":true}}
        // {where: {property: {op: value}}} bedrooms, location
        // filter={"where":{"disability":true, "homeless":false}}
        // lastly I attempted to use Node to query the repository instead of the controller but had issues getting access to the repos from the UI due to the react boilerplate src rules
        // if I had more time I might use Helper functions -- conditional flow handlers, etc. as another alternative
        for (let i = 0; i < response.length; i++) {
          if (response[i].senior === senior || response[i].disability === disabled || response[i].homeless === homeless) {
            const results = response.filter(response => response.senior === senior || response.disability === disabled || response.homeless === homeless);
            setWaitlists(results);
          }
        }
      })
      // added in error handling for the fetch call
      .catch(error => console.log(error));
  };

  // attempted to follow the mockup to the best of my ability, and added light styling

  return (
    <div className="App p-6">
      <div className="button-flex">
        <button className="button-primary-search">
          <a>Create Account</a>
        </button>
        <button className="button-primary-search">
          <a>Login</a>
        </button>
        <button className="button-primary-search">
          <a>Browse</a>
        </button>
      </div>
      <div className='row'>
        <div className='column'>
          <div className='blue-column'>
            <div className="search-criteria">
              <div className="clear-button">
                <button onClick={ClearP} className="button-primary-search">
                  <a>Clear Search</a>
                </button>
              </div>
              <br></br>
              <input value={bedrooms} onChange={onChangeB} className="text-input" type="number" placeholder="Bedrooms #" />
              <br></br>
              <input value={location} onChange={onChangeL} className="text-input" type="text" placeholder="States, Countries, Cities" />
              <br></br>
              <div className="labels">
                <label className="label">Senior 55+?
                  <input value={senior} name="senior" onChange={onChangeS} className="checkbox-input-1" type="checkbox" placeholder="Senior 55+?" />
                </label>
                <br></br>
                <label className="label">Disabled?
                  <input value={disabled} onChange={onChangeD} className="checkbox-input-2" type="checkbox" placeholder="Disabled Y/N" />
                </label>
                <br></br>
                <label className="label">Homeless?
                  <input value={homeless} onChange={onChangeH} className="checkbox-input-3" type="checkbox" placeholder="Experiencing Homelessness Y/N" />
                </label>
                <br></br>
              </div>
              <button onClick={filter} onSubmit={e => { handleSubmit(e) }} className="button-primary-search">
                <a>Search</a>
              </button>
            </div>
          </div>
        </div>
        <div className='column'>
          <div className='green-column'>
            <div className="space-y-3">
              <h1 className="text-navy text-4xl text-center"></h1>
              {waitlists && waitlists.map((w) => (
                <WaitlistCard key={id} waitlist={w} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
};

export default Search;