import React from "react";

const eligibilityCriteria = {
  disability: "Disabled",
  homeless: "Experiencing homelessness",
  senior: "Senior Citizen"
};

const WaitlistCard = ({ waitlist }) => {

  let id = waitlist.id;

  const eligibility = Object.keys(eligibilityCriteria).reduce((acc, ec) => {
    if (!waitlist[ec]) {
      return acc;
    }

    acc.push(eligibilityCriteria[ec]);
    return acc;
  }, []);
  return (
    <div>
    <div className="border-2 rounded-sm p-4">
      <div className="flex justify-between">
        <div>
          <h2 className="text-xl">{waitlist.waitlistName}</h2>
          <p className="text-sm">
            {eligibility.length > 0
              ? `Eligible if: ${eligibility.join(", ")}`
              : "All are eligible to apply"}
          </p>
          <div className="button-flex-details">
            <button className="button-primary">
              <a href={'/details/' + waitlist.id}>View More Details</a>
            </button>
          </div>
        </div>
        <div className="text-center">
          <p className="mb-3">
            {waitlist.isOpen ? (
              <span className="text-teal">Open</span>
            ) : (
              <span className="text-gold">Closed</span>
            )}
          </p>
          <p className="text-xs">Estimated wait </p>
          <p>{waitlist.avgWaitTimeMonths} Months</p>
        </div>
      </div>
    </div>
    </div>
  );
};

export default WaitlistCard;
