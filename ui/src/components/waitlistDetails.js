import React from "react";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import PropertiesCard from "../PropertiesCard";

// used functional components throughout to utilize hooks like useParams and useEffect

const WaitlistDetails = () => {

    // set the waitlists state for the initial API call for the waitlist objects to make it a little easier to work with & for seperation of concerns 

    let [waitlists, setWaitlists] = React.useState([]);

    // set the intiial state for the properties objects

    let [properties, setProperties] = React.useState([]);

    // got the unique id I added from the waitlist object to the route URL and retrieved its string value 

    const tempVar = useParams();
    console.log(tempVar);

    // parsed the string value to an integer

    let id = parseInt(tempVar.id);

    // called a use effect hook to make the API call to get the waitlist objects and set the state variable to the return of the API call
    // just used postman to test the API call and model the queries and code for the responses after it was made

    useEffect(() => {

        const url = 'http://localhost:3001/waitlists?filter={"include":["properties"]}';

        const fetchData = async () => {
            try {
                const response = await fetch(url);
                const json = await response.json();
                setWaitlists(json);
                // knowing I wanted seperation of concerns for the properties and waitlists I created a nested for loop/if statements to loop through and push out properties
                // into a new array that matched the unique id from the waitlist property so that I was working with the proper data sets
                let arr = [];
                for (let i = 0; i < json.length; i++) {
                    if (json[i].properties) {
                        for (let j = 0; j < json[i].properties.length; j++) {
                            if (json[i].properties[j].waitlistId === id) {
                                arr.push(json[i].properties[j]);
                            }
                        }
                    setProperties(arr);
                    }
                }
                // added in some error handling
            } catch (error) {
                console.log(error);
            }
        };
        fetchData();
    }, []);

    return (
        <div>
            <div className="parent-class">
                <div className="title-class-parent">
                    {waitlists.filter(waitlist => waitlist.id === id).map(waitlist => (
                        <h1 className="golden-title" key={id}>
                            {waitlist.waitlistName}
                        </h1>
                    ))}
                </div>
                <div className="title-class"></div>
                <div className="title-class"></div>
            </div>
            <br></br>
            <div className="child-class">
                <div className="child-flex">
                    {waitlists.filter(waitlist => waitlist.id === id).map(waitlist => (
                        <h2 className="status-class" key={id}>Status:    
                            {waitlist.isOpen ? (
                                <span className="text-teal">   Open</span>
                            ) : (
                                <span className="text-gold">Status: Closed</span>
                            )}
                        </h2>
                    ))}
                </div>
                <div className="space-flex"></div>
                <div className="child-flex">
                    {waitlists.filter(waitlist => waitlist.id === id).map(waitlist => (
                        <h2 key={id}>
                            Est. Wait Time: <span className="wait-time-class">{waitlist.avgWaitTimeMonths}</span>
                        </h2>
                    ))}
                </div>
                <br></br>
            </div>
            <br></br>
            <div className="eligible-flex">
                {waitlists.filter(waitlist => waitlist.id === id).map(waitlist => (
                    <h2 className="eligible-class" key={id}>
                        Eligible for:
                        {waitlist.disability === true ? (
                            <span className="text-teal">   Disabled</span>
                        ) : (
                            <span className="text-gold"></span>
                        )}
                        {waitlist.homeless === true ? (
                            <span className="text-teal">   Homeless</span>
                        ) : (
                            <span className="text-gold"></span>
                        )}
                        {waitlist.senior === true ? (
                            <span className="text-teal">   Senior 55+</span>
                        ) : (
                            <span className="text-gold"></span>
                        )}
                    </h2>
                ))}
            </div>
            <div className="description-flex">
            Waitlist Description: <span className="wait-time-class">The waitlist will be open until there are 50 people on the list.
            <br></br>
            It will reopen the following year.</span>
            </div>
            <br></br>
            <div className="properties-flex">
                <div className="properties-child">
                    Properties
                </div>
            </div>

            <br></br>

            <div className="row-cards">
                <div className="column-cards">
                    <div className="card">
                        {properties && properties.map((p) => (
                            <PropertiesCard key={p.waitlistId} properties={p} />
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default WaitlistDetails;