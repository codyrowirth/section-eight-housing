
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";

import Search from "./pages/search";
import Details from "./pages/details";

// seperated out the search page from the originial app.js file so that I could add routing into the app.js file
// added in routing for the "search" page and the "details" page which also has a unique id for each waitlist that it routes to and matches it for the purposes of displaying
// the correct properties

function App() {
  return (
    <>
      <Router>
        <Routes>
        <Route path="/" element={<Search />} /> 
        <Route path="/details/:id" element={<Details />} /> 
        </Routes>
      </Router>
    </>
  );
}

export default App;