First, install the dependencies:

    $ npm run setup

Then you can run the following to start both the API and the React app:

    $ npm start

That's it! The application should pop up in your browser automatically; if not, visit <http://localhost:3000>.

#### Working on front-end & back-end separately

If you'd prefer, you can install dependencies and start the two pieces separately. For the API:

    $ cd api
    $ npm install
    $ npm start

And for the React app:

    $ cd ui
    $ npm install
    $ npm start

Back end:
- [LoopBack](https://loopback.io/) v4

Front end:
- [React](https://reactjs.org/) v17
- [Tailwind](https://tailwindcss.com/) - There are some base theme colors added in tailwind.config.js
